package com.ttk.task.repository;

import com.ttk.task.domain.Launches;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LaunchesRepository extends JpaRepository<Launches, Integer> {
}
