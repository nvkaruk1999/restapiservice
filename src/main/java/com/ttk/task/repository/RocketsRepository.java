package com.ttk.task.repository;

import com.ttk.task.domain.Rockets;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RocketsRepository extends JpaRepository<Rockets, Integer> {
}
