package com.ttk.task.repository;

import com.ttk.task.domain.Links;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LinksRepository extends JpaRepository<Links, Integer> {
}
