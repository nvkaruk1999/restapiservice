package com.ttk.task.domain;

import javax.persistence.*;

@Entity
@Table(name = "launches")
public class Launches  {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String mission_name;

    private String launch_year;

    @OneToOne
    private Links links;

    public String getMission_name() {
        return mission_name;
    }

    public void setMission_name(String mission_name) {
        this.mission_name = mission_name;
    }

    public String getLaunch_year() {
        return launch_year;
    }

    public void setLaunch_year(String launch_year) {
        this.launch_year = launch_year;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public Integer getId() {
        return id;
    }
}
