package com.ttk.task.domain;

import javax.persistence.*;

@Entity
@Table(name = "rockets")
public class Rockets {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @Column(name = "rocket_id")
    private String rocket_id;

    public String getRocketId() {
        return rocket_id;
    }

    public void setRocketId(String rocketId) {
        this.rocket_id = rocketId;
    }

    public Integer getId() {
        return id;
    }
}
