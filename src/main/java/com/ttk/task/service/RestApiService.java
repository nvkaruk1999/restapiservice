package com.ttk.task.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ttk.task.domain.Launches;
import com.ttk.task.domain.Links;
import com.ttk.task.domain.Rockets;
import com.ttk.task.repository.LaunchesRepository;
import com.ttk.task.repository.LinksRepository;
import com.ttk.task.repository.RocketsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RestApiService {

    @Autowired
    private RocketsRepository rocketsRepository;

    @Autowired
    private LaunchesRepository launchesRepository;

    @Autowired
    private LinksRepository linksRepository;

    @Value("${api.allRockets}")
    private String apiRockets;

    @Value("${api.allLaunches}")
    private String apiLaunches;

    private RestTemplate restTemplate = new RestTemplate();

    public List<Rockets> listRocketId() throws IOException {
        ResponseEntity<String> response = this.restTemplate.getForEntity(apiRockets, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(response.getBody());
        List<Rockets> list = new ArrayList<>();

        for (JsonNode json : jsonNode) {
            Rockets rockets = new Rockets();
            rockets.setRocketId(json.get("rocket_id").asText());
            list.add(rockets);
            rocketsRepository.save(rockets);
        }

        return list;
    }

    public List<Launches> listLaunches(String rocketId) throws IOException {
        ResponseEntity<String> response = this.restTemplate.getForEntity(apiLaunches, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(response.getBody());
        List<Launches> list = new ArrayList<>();

        for (JsonNode json : jsonNode) {
            if (json.get("rocket").get("rocket_id").asText().equals(rocketId)) {
                List<String> flickr_images = new ArrayList<>();
                for (JsonNode element : json.get("links").get("flickr_images")) {
                    flickr_images.add(element.asText());
                }
                Launches launches = new Launches();
                Links links = new Links();
                launches.setMission_name(json.get("mission_name").asText());
                launches.setLaunch_year(json.get("launch_year").asText());

                links.setMission_patch(json.get("links").get("mission_patch").asText());
                links.setMission_patch_small(json.get("links").get("mission_patch_small").asText());
                links.setReddit_campaign(json.get("links").get("reddit_campaign").asText());
                links.setReddit_launch(json.get("links").get("reddit_launch").asText());
                links.setReddit_recovery(json.get("links").get("reddit_recovery").asText());
                links.setReddit_media(json.get("links").get("reddit_media").asText());
                links.setPresskit(json.get("links").get("presskit").asText());
                links.setArticle_link(json.get("links").get("article_link").asText());
                links.setWikipedia(json.get("links").get("wikipedia").asText());
                links.setVideo_link(json.get("links").get("video_link").asText());
                links.setYoutube_id(json.get("links").get("youtube_id").asText());
                links.setFlickr_images(flickr_images);

                launches.setLinks(links);
                linksRepository.save(links);
                launchesRepository.save(launches);
                list.add(launches);
            }
        }

        return list;
    }
}
